# -*- coding: utf-8 -*-
"""
Created on Mon Dec 13 14:02:23 2021

Test between Euler-Mayurama and Ito diffusion

@author: gomel
"""
import numpy as np
import matplotlib.pyplot as plt

def f1(y,r=1.): 
    """pitchfork equation"""
    return r*(y-0.3)-(y)**3

def g1(x,sig=0.5): #
    return sig

def dW(delta_t,n):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t),size=n)

def obs_setup(n,sig):
    x=np.zeros(n)
    x0=np.random.normal(0,sig,1)
    x[0]=x0
    return x

def r_sine(dt,t,T,oscillations,c_v,rs):
    n=int(T/dt)
    "max speed of change: vel"
    for i in range(n-1):
        if int(t[i]/(T/oscillations)%2)==0:
            vel=c_v
        else: vel=-c_v
    "f=A sin(w t) has a max speed of Aw, so "
    period=T/oscillations
    omega=2*np.pi/period
    rs=vel/omega*np.sin(omega*t)
    return rs



"time array variales"
dt=.0005 # time step
T=600
n=int(T/dt)
t=np.linspace(0.,T,n) #time vector

"noise"
sig=dt*10

"observable array setup"
x=obs_setup(n,sig) #euler
"Ito observable for detrending"
y=obs_setup(n,sig) #ito
"Ito observable for detrending"
x_det=obs_setup(n,sig) #deterministic


rs=np.linspace(-2,2,n)
oscillations=2
c_v=0.1
rs=r_sine(dt,t,T,oscillations,c_v,rs)

"integration"
for i in range(n-1):
    r=rs[i]
    x[i+1]=x[i]+dt*f1(x[i] ,r)+g1(x[i],sig)*dW(dt,1)
    x_det[i+1]=x_det[i]+dt*f1(x_det[i] ,r)

"integration ito"
DW=dW(dt,n+1)

for i in range(n-1):
    DEW=(DW[i+1]-DW[i])
    r=rs[i]
    yy=y[i]+dt*f1(y[i] ,r)+g1(y[i],sig)*np.sqrt(dt)
    y[i+1]=y[i]+dt*f1(y[i] ,r)+g1(y[i],sig)*DEW + 0.5*(g1(yy,sig)-g1(y[i],sig))*(DEW**2-dt)*dt**(-0.5)
    
    #%%
# ax1=plt.subplot(3,3,(1,2))


fig=plt.figure()
ax=fig.add_subplot(1,1,1)
ax.plot(t,x,'r',label='Euler-Mayorama',alpha=0.7)
ax.plot(t,y,'b',label='Ito',lw=1.4,alpha=0.7)
ax.plot(t,x_det,'--c',label='Deterministic',alpha=1)
ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis
ax2.plot(t,rs,'k',alpha=0.4)
plt.ylabel('r')
plt.legend()
plt.xlabel('time')
right_inset_ax = fig.add_axes([.55, .18, .3, .3])
right_inset_ax.plot(t,x,'r',alpha=0.7)
right_inset_ax.plot(t,y,'b',alpha=0.7)
plt.plot(t,x_det,'--c',alpha=1)
right_inset_ax.set_xlim([140,160])
right_inset_ax.set_ylim([0.,0.3])

