# -*- coding: utf-8 -*-
"""
Created on Mon Oct 24 10:05:01 2022

@author: gomel
"""

import sys
sys.path.append("C:/Users/gomel/Documents/global_python_codes/Metrics_thresholds_boots") # go to parent dir
import matplotlib.pyplot as plt    
plt.rcParams.update({
    "text.usetex": False,
    "font.family": "serif"    
})
        


def make_stats(tspan,t,ts,varwin,ewswin,sm=False):
    '''
    This calculates all the variables for the analysis of the EWS.
    
    Parameters
    ----------
    tspan : TYPE
        DESCRIPTION.
    t : TYPE
        DESCRIPTION.
    ts : TYPE
        DESCRIPTION.
    varwin : TYPE
        DESCRIPTION.
    ewswin : TYPE
        DESCRIPTION.

    Returns
    -------
    list
        DESCRIPTION.

    '''
    import metrics
    import numpy as np
    from scipy.stats import skew, kurtosis
    
    rw=ewswin
    data=ts.state['state'].values
   # ts.detrend(method='Gaussian',bandwidth=bw, span=varwin)
   
    if sm==True:
        residuals=ts.state['residuals'].values   
        data=residuals

    #ts.compute_var(rolling_window=rw)
    ts.compute_auto(lag=1, rolling_window=rw)
    ts.compute_auto(lag=2, rolling_window=rw)
    #ts.compute_skew(rolling_window=rw)
    #ts.compute_kurt(rolling_window=rw)
    ts.compute_ktau()
    #ewswin=varwin
    ac_labels = [s for s in ts.ews.columns if s[:2]=='ac']
    #smooth=ts.state['smoothing'].values
    ac1=ts.ews[ac_labels[0]].values
    ac2=ts.ews[ac_labels[1]].values
    #var_ews=ts.ews['variance'].values
    #sk=ts.ews['skew'].values
    #kur=ts.ews['kurtosis'].values
    rtw=np.nan*np.ones(len(ac1))
    hogg2=np.nan*np.ones(len(ac1))
    sk=np.nan*np.ones(len(ac1))
    kur=np.nan*np.ones(len(ac1))
    var=np.nan*np.ones(len(ac1))
    moors=np.nan*np.ones(len(ac1))
    RTWc=np.ones(len(ac1))+1j*np.ones(len(ac1))

    tidx = t[~np.isnan(ac1)]
    RTWc=[metrics.RTW_c(data[j:tidx[j]]) for j  in  range(len(tidx)) ] 
    hogg2=[metrics.Hogg2(data[j:tidx[j]]) for j  in  range(len(tidx)) ] 
    sk=np.array([skew(data[j:tidx[j]]) for j  in  range(len(tidx)) ] )
    kur=np.array([kurtosis(data[j:tidx[j]]) for j  in  range(len(tidx)) ]) 
    var=np.array([np.var(data[j:tidx[j]]) for j  in  range(len(tidx)) ] )
    moors=[metrics.Moors(data[j:tidx[j]]) for j  in  range(len(tidx)) ] 
    rtw=np.max((np.real(RTWc),np.imag(RTWc)),axis=0)
    return [tidx,ac_labels,ac1,ac2,var,sk,kur,rtw,hogg2,moors,RTWc]


########################################################################################
######################################################################################
#add datalen
def stats_plot(bif_idx,tidx,ind_f,s,rtw,RTWc,moors,hogg2,ac1,ku_ews,sk_ews,ac2,var_ews,residuals,res,smooth,smoothing,ewswin,dt=1,fz=26):
    '''
    Code for plotting the statistics on EWS

    Parameters
    ----------
    bif_idx : TYPE
        DESCRIPTION.
    tidx : TYPE
        DESCRIPTION.
    ind_f : TYPE
        DESCRIPTION.
    rtw : TYPE
        DESCRIPTION.
    kr3 : TYPE
        DESCRIPTION.
    kr2 : TYPE
        DESCRIPTION.
    ac1 : TYPE
        DESCRIPTION.
    ku_ews : TYPE
        DESCRIPTION.
    sk_ews : TYPE
        DESCRIPTION.
    ac2 : TYPE
        DESCRIPTION.
    var_ews : TYPE
        DESCRIPTION.
    residuals : TYPE
        DESCRIPTION.
    res : TYPE
        DESCRIPTION.
    smoothing : TYPE
        DESCRIPTION.
    ewswin : TYPE
        DESCRIPTION.

    Returns
    -------
    None.

    '''
    import matplotlib.pyplot as plt
    import scipy.stats as st
    import numpy as np
    from matplotlib import ticker
    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True) 
    formatter.set_powerlimits((-1,1)) 
    
    plt.rcParams.update({'font.size': 22})
    plt.rcParams.update({
    "text.usetex": False,
    "font.family": "serif"
    })
    CB_color_cycle = ['#377eb8', '#ff7f00', '#4daf4a',
                  '#f781bf', '#a65628', '#984ea3',
                  '#999999', '#e41a1c', '#dede00']
    
    
    c1=(221/256,170/256,51/256)
    c2=(187/256,85/256,102/256)
    c3=(0,68/256,136/256)
    cols=(c1,c2,c3)

    trans=bif_idx
    fig, ax = plt.subplots(6, 2, sharex='col',figsize=(20,22))
    plt.rc('font',size=fz)
    plt.subplots_adjust(hspace=0.05)
    plt.subplots_adjust(wspace=0.4)
    
    '''estimatios and other things'''
    
    ac1=ac1[tidx]
    ac2=ac2[tidx]
    
    Mac=-(ac1-1)/dt
    vac=-var_ews/(ac1-1)
    Mvar=-s**2/(2*var_ews)


    axs=ax[0,0]
    tau, p_value = st.kendalltau(rtw[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],rtw[:bif_idx-ewswin],linewidth=2,
             label=r'$k_{\tau}=%.2f$' %(tau))
    axs2=axs.twinx()
    axs2.plot(tidx,rtw,linewidth=2,alpha=0.4)
    if smoothing==True:
        axs.set_title('Gaussian detrending')
    else:
        axs.set_title('No detrending')


    

    axs.set_ylabel('RTW')
    axs=ax[2,0]
    tau, p_value = st.kendalltau(moors[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],moors[:bif_idx-ewswin],linewidth=2,
             label=r'$k_{\tau}=%.2f$' %(tau))
    axs.set_ylabel('Moors')
    axs2=axs.twinx()
    axs2.plot(tidx,moors,linewidth=2,alpha=0.4)
    axs=ax[1,0]
    tau, p_value = st.kendalltau(np.abs(RTWc[:bif_idx-ewswin]), tidx[:bif_idx-ewswin])
    axs.plot(tidx,np.abs(RTWc),linewidth=2,label=r'$k_{\tau}=%.2f$' %(tau))
    axs2=axs.twinx()
    axs2.plot(tidx,np.unwrap(np.angle(RTWc)),color=c2,linewidth=2,alpha=0.4)
    axs.set_ylabel('|RTWc|')
    axs2.set_ylabel('angle(RTWc)')

    axs=ax[3,0]
    tau, p_value = st.kendalltau(hogg2[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],hogg2[:bif_idx-ewswin],linewidth=2,
             label=r'$k_{\tau}=%.2f$' %(tau))
    axs2=axs.twinx()
    axs2.plot(tidx,hogg2,linewidth=2,alpha=0.4)
    axs.set_ylabel('Hogg')
    axs=ax[4,0]
    tau, p_value = st.kendalltau(ku_ews[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],ku_ews[:bif_idx-ewswin],linewidth=2,
             label=r'$k_{\tau}=%.2f$' %(tau))
    axs2=axs.twinx()
    axs2.plot(tidx,ku_ews,linewidth=2,alpha=0.4)
    axs.set_ylabel('Kurtosis')
    axs=ax[5,0]
    tau, p_value = st.kendalltau(sk_ews[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],sk_ews[:bif_idx-ewswin],linewidth=2,
             label=r'$k_{\tau}=%.2f$' %(tau))
    axs2=axs.twinx()
    axs2.plot(tidx,sk_ews,linewidth=2,alpha=0.4)
    axs.set_ylabel('Skewness')
    
    axs.set_xlabel('Integration step')

    axs=ax[0,1]
    tau, p_value = st.kendalltau(ac1[:bif_idx-ewswin], ac2[:bif_idx-ewswin])
    axs.set_title(r'$k^{1,2}_{\tau}=%.2f$' %(tau))
    
    tau, p_value = st.kendalltau(ac1[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],ac1[:bif_idx-ewswin],color=CB_color_cycle[2],
             linewidth=2,label=r'$k^1_{\tau}=%.2f$' %(tau))
    tau, p_value = st.kendalltau(ac2[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],ac2[:bif_idx-ewswin],color=CB_color_cycle[4],
             linewidth=2,label=r'$k^2_{\tau}=%.2f$' %(tau))
    axs2=axs.twinx()
    axs2.plot(tidx,ac1,color=CB_color_cycle[2],linewidth=2,alpha=0.4)
    axs2.plot(tidx,ac2,color=CB_color_cycle[4],linewidth=2,alpha=0.4)
    axs.set_ylabel('Lag$_{1,2}$')
    axs=ax[1,1]
    tau, p_value = st.kendalltau(var_ews[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],var_ews[:bif_idx-ewswin],color=CB_color_cycle[1],
             linewidth=2,label=r'$k_{\tau}=%.2f$' %(tau))
    #axs.plot(ind,var_mine)
    axs.set_ylabel('Variance')
    axs.yaxis.set_major_formatter(formatter) 
    axs2=axs.twinx()
    axs2.plot(tidx,var_ews,linewidth=2,color=CB_color_cycle[1],alpha=0.4)
    axs2.yaxis.set_major_formatter(formatter) 
    axs=ax[2,1]
    tau, p_value = st.kendalltau(Mac[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],Mac[:bif_idx-ewswin],color=CB_color_cycle[2],
             linewidth=2,label=r'$k_{\tau}=%.2f$' %(tau))
    tau, p_value = st.kendalltau(-Mvar[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx[:bif_idx-ewswin],-Mvar[:bif_idx-ewswin],color=CB_color_cycle[1],
             linewidth=2,label=r'$k_{\tau}=%.2f$' %(tau))
    axs2=axs.twinx()
    axs2.plot(tidx,Mac,color=CB_color_cycle[2],lw=2,alpha=0.4)
    axs2.plot(tidx,-Mvar,color=CB_color_cycle[1],lw=2,alpha=0.4)
    axs.set_ylabel('M')
    axs=ax[3,1]
    tau, p_value = st.kendalltau(vac[:bif_idx-ewswin], tidx[:bif_idx-ewswin])
    axs.plot(tidx,vac,color=CB_color_cycle[3],linewidth=2,label=r'$k_{\tau}=%.2f$' %(tau))
    axs.set_ylabel('-Var/(Lag-1)')
    axs.set_yscale('log')



    if smoothing==True:
        axs=ax[4,1]
        axs.plot(ind_f[:bif_idx],residuals[:bif_idx],color='gray',linewidth=2)
        axs.set_ylabel('Residuals')
        axs.yaxis.set_major_formatter(formatter) 
        axs2=axs.twinx()
        axs2.plot(ind_f,residuals,linewidth=2,color='gray',alpha=0.4)
        axs2.yaxis.set_major_formatter(formatter) 
    else:
        fig.delaxes(ax[4,1])

        
    axs=ax[5,1]
    axs.plot(ind_f[:bif_idx],res[:bif_idx],'k',linewidth=2)
    if smoothing==True: axs.plot(ind_f[:bif_idx],smooth[:bif_idx],'r',
                                 linewidth=2,zorder=10)  
    axs2=axs.twinx()
    axs2.plot(ind_f,res,'k',lw=2,alpha=0.4)
    if smoothing==True:
        axs2.plot(ind_f,smooth,'r',linewidth=2,alpha=0.4,zorder=4)
    axs.set_ylabel('Trayectory')   
    axs.set_xlabel('Integration step')

    for axs in ax.ravel():   
        ylim=axs.get_ylim()
        axs.set_xlim([0,len(res)])

        axs.plot([trans,trans],ylim,'-',linewidth=4,color='gray',alpha=0.3)
        axs.plot([trans-ewswin,trans],[ylim[1],ylim[1]],'|-',markersize=15,
                 markeredgewidth=4,color=c2,lw=4)
        axs.annotate('EWS (%.1i)'%(ewswin),
                    xy=(trans-ewswin*1.1, ylim[1]), xycoords='data',
                    horizontalalignment='right', verticalalignment='center',
                    fontsize=12)
        axs.legend(loc='best')      
    plt.rc('font',size=fz)



    return fig

#########################################################################
def plot_detrend(fig, ewswin,varwin,t_rm,dt,bw,trans,fz=26):
    import matplotlib.pyplot as plt
    from scipy import signal
    import numpy as np
    plt.rcParams.update({
        "text.usetex": False,
        "font.family": "serif"    
    })
        
    c1=(221/256,170/256,51/256)
    c2=(187/256,85/256,102/256)
    c3=(0,68/256,136/256)
    
    ax_list = fig.axes
    ax=ax_list[0]
    ylim=ax.get_ylim()  
    yvwin=(ylim[1]-ylim[0])*0.1    
    ywin=ylim[0]+(ylim[1]-ylim[0])*0.15   
    ytcor=ylim[0]+(ylim[1]-ylim[0])*0.05   
    xlim=ax.get_xlim()  
    xoffset=0.02
    xof=xlim[0]+xoffset*(xlim[1]-xlim[0])
    plt.plot([trans-ewswin,trans],[ylim[1],ylim[1]],'|-',color=c2,lw=4,markersize=15,markeredgewidth=4,
             label='EWS window',zorder=10)
    
    gwin=signal.windows.gaussian(varwin, std=bw)#Gaussian window

    leg = ax.legend(['Detrending window (%.1i)' %(varwin),'Detrending window (%.1i)' %(varwin),'asd','asdsa','Initial return rate (%.1i)'%(t_rm/dt)],fontsize=fz-1, loc='best', frameon=False)
    plt.draw()
    p = leg.get_window_extent()
    p0 = ax.transData.inverted().transform(p.p0)
    p0[1]=p0[1]-(np.max(gwin)-np.min(gwin))
    
    plt.plot([p0[0]+xof,p0[0]+xof+varwin],[p0[1]+yvwin,p0[1]+yvwin],'|-',markersize=15,markeredgewidth=4,
             color=c3,lw=4,label='Detrending window')
    plt.plot([p0[0]+xof,p0[0]+xof+t_rm/dt],[p0[1]+0*ytcor,p0[1]+0*ytcor],'|-',markersize=15,markeredgewidth=4,
             color=c3,lw=4,label='Initial correlation')
    gx=p0[0]+xof+np.arange(0,len(gwin),1)
    plt.plot(gx,gwin-np.min(gwin)+p0[1]+yvwin,color=c3)
  
    
    
    ax.annotate('EWS window (%.1i) \n (%.1f) s'%(ewswin,ewswin*dt),
                xy=(trans+ewswin*0.4, ylim[1]), xycoords='data',
                horizontalalignment='left', verticalalignment='center',
                fontsize=fz-2)
    ax.annotate('Detrending window (%.1i)'%(varwin),
                xy=(p0[0]+xof+varwin*1.4, p0[1]+yvwin), xycoords='data',
                horizontalalignment='left', verticalalignment='center',
                fontsize=fz-2)
    ax.annotate('Initial return rate (%.1i)'%(t_rm/dt),
                xy=(p0[0]+xof+t_rm/dt*1.4, p0[1]+0*ytcor), xycoords='data',
                horizontalalignment='left', verticalalignment='center',
                fontsize=fz-2)
    
    ax.plot([trans,trans],ylim,'-',linewidth=4,color='gray',alpha=0.3,label='Bifurcation')
    ax.set_ylabel(r'$x$')
    ax.set_xlabel('Integration step')
    plt.rc('font',size=fz)
    
    ax.get_legend().remove()

    return fig
###############################################################

def plot_detrend_data(fig, ewswin,varwin,bw,trans,fz=26):
    import matplotlib.pyplot as plt
    from scipy import signal
    import numpy as np

    
    c1=(221/256,170/256,51/256)
    c2=(187/256,85/256,102/256)
    c3=(0,68/256,136/256)
    
    ax_list = fig.axes
    plt.rc('font',size=fz)

    ax=ax_list[0]
    ylim=ax.get_ylim()  
    yvwin=ylim[0]+(ylim[1]-ylim[0])*0.15    
    ywin=ylim[0]+(ylim[1]-ylim[0])*0.15   
    ytcor=ylim[0]+(ylim[1]-ylim[0])*0.05   
    plt.plot([trans-ewswin,trans],[ylim[1],ylim[1]],'|-',color=c2,lw=4,markersize=15,markeredgewidth=4,label='EWS window',zorder=10)
    plt.plot([0,varwin],[yvwin,yvwin],'|-',markersize=15,markeredgewidth=4,color=c3,lw=4,label='Detrending window')
    gwin=signal.windows.gaussian(varwin, std=bw)
    plt.plot(gwin-np.min(gwin)+yvwin,color=c3)
    
    
    
    
    
    ax.annotate('EWS window (%.1i)'%(ewswin),
                xy=(trans-ewswin*1.2, ylim[1]), xycoords='data',
                horizontalalignment='right', verticalalignment='center',
                fontsize=12)
    ax.annotate('Detrending window (%.1i)'%(varwin),
                xy=(varwin*1.2, yvwin), xycoords='data',
                horizontalalignment='left', verticalalignment='center',
                fontsize=12)

    ax.plot([trans,trans],ylim,'-',linewidth=4,color='gray',alpha=0.3,label='Bifurcation')

    return fig


#####################################################################

def ac12_plot(ac1,ac2,tidx,bif_idx):
    import matplotlib.pyplot as plt
    from scipy import signal
    import numpy as np
    from matplotlib.colors import ListedColormap, BoundaryNorm
    from matplotlib import cm
    
    
    cmap = cm.get_cmap('plasma')
    pnorm=(tidx[:bif_idx]-np.min(tidx[:bif_idx]))/np.max(tidx[:bif_idx]-np.min(tidx[:bif_idx]))
    np.sort(pnorm)
    plot_t=tidx[:bif_idx]
    norm = BoundaryNorm(np.sort(plot_t[::np.int64(len(plot_t)/255)]), cmap.N)
    fig=plt.figure(figsize=(10,10))
    ax=fig.add_subplot(211)
    plt.scatter(ac1[plot_t],ac2[plot_t],color=cmap(np.int64(255*pnorm)))
    #cbar_ax = fig.add_axes([0.9, 0.05, 0.05, 0.9])
    cbar=plt.colorbar(cm.ScalarMappable(norm=norm, cmap=cmap),label='Time',ticks=np.sort(plot_t[::np.int64(len(plot_t)/5)]))
    #cbar.set_label('Time', rotation=90,fontsize=16,labelpad=3)
    ax.set_facecolor((170/255,170/255 ,170/255,0.8 ))
    ax.set_ylabel('Lag$_{2}$')
    ax.set_xlabel('Lag$_{1}$')
    ax=fig.add_subplot(212)

    ax.plot(tidx,ac2[tidx]/ac1[tidx])
    ax.set_ylabel('Lag$_{2}$/Lag$_{1}$')
    ylim=ax.get_ylim()
    ax.plot([bif_idx,bif_idx],ylim,'-',linewidth=4,color='gray',alpha=0.3,label='Bifurcation')


    return fig