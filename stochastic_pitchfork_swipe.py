# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 12:36:45 2020
SDE from 
'pitchfork' ry-y^3


Swiping r at some speed

It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 

DUNNO WHY THIS DOESN'T WORK
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
import scipy.stats as st

def f1(y,r=1.):
    f=r*y-y**3
    # f=r*(y-2)*(y-1)*(y-3)#r*y*(y**2-1)
    return f

def g1(x,sig=0.5):
    return sig*x

def dW(delta_t):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))

def r_seesaw(dt,t,T,oscillations,c_v,rs):
    n=int(T/dt)
    for i in range(n-1):
        if int(t[i]/(T/oscillations)%2)==0:
            vel=c_v
        else: vel=-c_v
        rs[i+1]=rs[i]+dt*vel
    return rs


def r_sine(dt,t,T,oscillations,c_v,rs):
    n=int(T/dt)
    "max speed of change: vel"
    for i in range(n-1):
        if int(t[i]/(T/oscillations)%2)==0:
            vel=c_v
        else: vel=-c_v
    "f=A sin(w t) has a max speed of Aw, so "
    period=T/oscillations
    omega=2*np.pi/period
    rs=vel/omega*np.sin(omega*t)
    return rs

def sde_euler_maruyama(f,g,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])+g(x[i],argsg)*np.random.normal(loc=0.0, scale=np.sqrt(dt))
    return x,t



"time array variales"
dt=.025 # time step
T=60
n=int(T/dt)
t=np.linspace(0.,T,n) #time vector

"noise"
sig=0.5

"observable array setup"
x=np.zeros(n)
x0=np.random.normal(0,0.002,1)
x[0]=x0
"deterministic observable for detrending"
x_analit=np.zeros(n)
x_analit[0]=x0

"control parameter setup"
rs=np.zeros(n)
r0=-10
rs[0]=r0
max_c=20-r0
oscillations=3
c_v=(max_c)/(T/oscillations)

"rs as a function of time"
rs=r_seesaw(dt,t,T,oscillations,c_v,rs)
rs=r_sine(dt,t,T,oscillations,c_v,rs)
rs=np.ones_like(rs)*5  
#%%
y=x
y2=y
for i in range(n-1):
    y[i+1]=y[i]+dt*f1(y[i] ,rs[0])+g1(y[i],sig)*dW(dt)
    y2[i+1]=y2[i]+dt*f1(y2[i] ,rs[0])
fig=plt.figure()
ax=fig.add_subplot(2,1,1)
plt.suptitle('Pitchfork y')
ax.plot(t,y,lw=2,color='red')    
ax=fig.add_subplot(2,1,2)
ax.plot(t,y-y2,lw=2,color='red')    

#%%
r_crit=0.
r_ix = [idx for idx, r in enumerate(rs) if r > r_crit]
#%%
    # x,t=sde_euler_maruyama(f1,g1,x0=1,argsf=(r,c,h,k),argsg=(sig))
for i in range(n-1):
    r=rs[i]
    x[i+1]=x[i]+dt*f1(x[i] ,r)+dW(dt)*g1(x[i],sig)+np.random.rand()
    x_analit[i+1]=x_analit[i]+dt*f1(x_analit[i] ,r)
   
    
    # ax.plot(t,x,lw=2,color=vir((rs[-1]-r)/(rs[-1]-rs[0])))
    #%%
fig=plt.figure()
ax=fig.add_subplot(2,1,1)
plt.suptitle('Pitchfork SDE critical slowing down')
ax.plot(t,x,lw=2,color='red')    
ax.plot(t[r_ix],x[r_ix],'.',lw=0.1,color='blue')
ax.plot(t,x_analit,lw=0.5,color='k')
ax.set_xlabel('Time')
ax.set_ylabel('$y^*$')
ax2=ax.twinx()
# ax2=fig.add_subplot(122)
ax2.plot(t,rs,'b',lw=2.5,alpha=0.2)    
ax2.yaxis.label.set_color('blue')
ax2.tick_params(axis='y', colors='blue')
 # cbar=plt.colorbar(sm)
# cbar.set_label('r')
ax2.set_ylabel('$r$')

ax=fig.add_subplot(2,1,2)
ax.plot(t,x-x_analit,lw=0.5,color='k')
ax.set_xlabel('Time')
ax.set_ylabel('$y detrended$')
# ax2=ax.twinx()
# # ax2=fig.add_subplot(122)
# ax2.plot(t,rs,'b',lw=2.5,alpha=0.2)    
# ax2.yaxis.label.set_color('blue')
# ax2.tick_params(axis='y', colors='blue')
#  # cbar=plt.colorbar(sm)
# # cbar.set_label('r')
# ax2.set_ylabel('$r$')
#%%
win_amo=100
win=int(len(t)/win_amo)
means=np.zeros(win_amo)
t_metr=np.zeros(win_amo)
stds=np.zeros(win_amo)
RTW=np.zeros(win_amo)
kur=np.zeros(win_amo)
kr2=np.zeros(win_amo)
kr3=np.zeros(win_amo)
rri=np.zeros(win_amo)

for n in range(win_amo):
    means[n]=np.mean(x[n*win:(n+1)*win])
    t_metr[n]=np.mean(t[n*win:(n+1)*win])
    stds[n]=np.var(x[n*win:(n+1)*win])
    # RTW[n]=metrics.RTW_max(x[n*win:(n+1)*win])
    kur[n]=st.kurtosis(x[n*win:(n+1)*win])
    kr2[n]=metrics.kr2(x[n*win:(n+1)*win])
    kr3[n]=metrics.kr3(x[n*win:(n+1)*win])
    rri[n]=metrics.metric_rms_tld(x[n*win:(n+1)*win])
    # brtw=boots.bootstrap_func_len(metrics.RTW_max, x[n*win:(n+1)*win], 20)
    RTW[n]=metrics.RTW_max(x[n*win:(n+1)*win])
    # print(n)
    #%%
fig=plt.figure()
ax2=fig.add_subplot(2,3,1)
plt.suptitle('Pitchfork SDE  window size= %i, t= %.2f' %(win,t[win]) )
ax2.plot(t_metr,rri,'r',lw=2,label='RR1') 
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,2)
ax2.plot(t_metr,stds,'r',lw=2,label='Variance')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,3)
ax2.plot(t_metr,RTW,'r',lw=2,label='RTW')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,4)
ax2.plot(t_metr,kur,'r',lw=2,label='kurtosis')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,5)
ax2.plot(t_metr,kr2,'r',lw=2,label='kr2')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,6)
ax2.plot(t_metr,kr3,'r',lw=2,label='kr3')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])   
    



# v_r=12
# rs_max=r0+v_r*T
# v_r=-2*r0/T #simmetric r0