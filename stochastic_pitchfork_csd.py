# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 12:36:45 2020


SDE from 
'pitchfork'

It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots

def f1(y,r=1.):
    return r*y-y**3

def g1(x,sig=0.5):
    return sig*x

def dW(delta_t):

    """Sample a random number at each call."""

    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))


def sde_euler_maruyama(f,g,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])+g(x[i],argsg)*np.random.rand()
    return x,t


fig,ax=plt.subplots(1,1)
plt.suptitle('Pitchfork SDE critical slowing down')
rs=np.linspace(-4,4,80)
means=np.zeros_like(rs)

vir=cm.get_cmap('viridis',12)
sm=plt.cm.ScalarMappable(cmap='viridis',norm=plt.Normalize(vmin=rs[0],vmax=rs[-1]))
for j,r in enumerate(rs):
        
    # r=10
    # # c=0.04
    # h=.5
    # k=100
    sig=0.05
    x0=np.random.normal(0,0.002,1)
    
    dt=.025 # time step
    T=100
    n=int(T/dt)
    
    x=np.zeros(n)
    x[0]=x0
    
    
    # x,t=sde_euler_maruyama(f1,g1,x0=1,argsf=(r,c,h,k),argsg=(sig))
    t=np.linspace(0.,T,n) #time vector
    
    for i in range(n-1):
        x[i+1]=x[i]+dt*f1(x[i] ,r)+g1(x[i],sig)*dW(dt)

    means[j]=np.mean(x)
    ax.plot(t,x,lw=2,color=vir((rs[-1]-r)/(rs[-1]-rs[0])))
    
cbar=plt.colorbar(sm)
cbar.set_label('r')
plt.title('Noise: $\sigma x$')
ax.set_xlabel('Time')
ax.set_ylabel('$y^*$')