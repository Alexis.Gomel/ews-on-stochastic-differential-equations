# -*- coding: utf-8 -*-
"""
Created on Sun Dec 12 14:37:02 2021

Detrending test

@author: gomel
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
import threshold
import metrics 
import astropy.stats as astrost
import boots
from scipy.signal import  savgol_filter 


def f1(y,r=1.): 
    """pitchfork equation"""
    return r*(y-0.3)-(y)**3

def g1(x,sig=0.5): #
    return sig

def dW(delta_t,n):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t),size=n)

def obs_setup(n,sig):
    x=np.zeros(n)
    x0=np.random.normal(0,sig,1)
    x[0]=x0
    return x


def r_sine(dt,t,T,oscillations,c_v,rs):
    n=int(T/dt)
    "max speed of change: vel"
    for i in range(n-1):
        if int(t[i]/(T/oscillations)%2)==0:
            vel=c_v
        else: vel=-c_v
    "f=A sin(w t) has a max speed of Aw, so "
    period=T/oscillations
    omega=2*np.pi/period
    rs=vel/omega*np.sin(omega*t)
    return rs




"time array variales"
dt=.005 # time step
T=600
n=int(T/dt)
t=np.linspace(0.,T,n) #time vector

"noise"
sig=0.05

"observable array setup"
x=obs_setup(n,sig)
"deterministic observable for detrending"
y=obs_setup(n,sig)


rs=np.linspace(-3,3,n)
oscillations=3
c_v=0.1
rs=r_sine(dt,t,T,oscillations,c_v,rs)
r_crit=0.
r_ix = [idx for idx, r in enumerate(rs) if r > r_crit]

"integration"
for i in range(n-1):
    r=rs[i]
    # x[i+1]=x[i]+dt*f1(x[i] ,r)+g1(x[i],sig)*dW(dt)
    y[i+1]=y[i]+dt*f1(y[i] ,r)

"integration ito"
DW=dW(dt,n+1)

for i in range(n-1):
    DEW=(DW[i+1]-DW[i])
    r=rs[i]
    xx=x[i]+dt*f1(x[i] ,r)+g1(x[i],sig)*np.sqrt(dt)
    x[i+1]=x[i]+dt*f1(x[i] ,r)+g1(x[i],sig)*DEW + 0.5*(g1(xx,sig)-g1(x[i],sig))*(DEW**2-dt)*dt**(-0.5)
    

"detrending"
analitical_DET=x-y

Half_box_data=100
Rwin=2*Half_box_data #detrending data
Rwin_step=1
win=np.zeros(Rwin)
effective_lenght=n-Rwin
n_win=int(effective_lenght/Rwin_step)
T_rolling_window=Rwin*dt
ix=np.linspace(Rwin,(n_win-1)*Rwin_step+Rwin,Rwin_step)
t_win=np.zeros(n_win)
t_win=t[Half_box_data:-Half_box_data]
# for k in range(n_win):
#     t_win[k]=t[k*Rwin_step+Rwin]

#%%
mean_detrend=np.zeros(n_win)
median_detrend=np.zeros(n_win)
median_gaussian=np.zeros(n_win)
savgol_detrend=np.zeros(n_win)
gaus_box=20*Half_box_data
g_weights=np.exp(-(np.linspace(-Half_box_data,Half_box_data,Rwin))**2/(gaus_box**2))

# plt.figure()
# plt.plot(t_win[Rwin_step:Rwin_step+Rwin],g_weights,'.')

#%%
for k in range(n_win):
        mean_detrend[k]=np.mean(x[k*Rwin_step:k*Rwin_step+Rwin])
        median_detrend[k]=np.median(x[k*Rwin_step:k*Rwin_step+Rwin])
        median_gaussian[k]=np.median(np.multiply(g_weights,x[k*Rwin_step:k*Rwin_step+Rwin]))

sav_det=savgol_filter(x, 2*Half_box_data+1, 2)
x_sav=x-sav_det
x_median=x[Half_box_data:-Half_box_data]-median_detrend
x_mean=x[Half_box_data:-Half_box_data]-mean_detrend
x_median_g=x[Half_box_data:-Half_box_data]-median_gaussian
#%%
fig=plt.figure()
ax=fig.add_subplot(4,1,1)
ax.plot(t,x,lw=2)
ax.plot(t[r_ix],x[r_ix],lw=2)
ax.plot(t_win,median_detrend,lw=1)
ax.plot(t_win,mean_detrend,lw=1)
ax.plot(t,y,lw=0.5,color='black')
ax2=ax.twinx()
# ax2=fig.add_subplot(122)
ax2.plot(t,rs,'b',lw=2.5,alpha=0.3)    
ax2.plot([t[0],t[-1]],[r_crit,r_crit],'--b',lw=1,alpha=0.2)    
ax2.yaxis.label.set_color('blue')
ax2.tick_params(axis='y', colors='blue')
ax2.set_ylabel('$r$')

ax=fig.add_subplot(4,2,3)
# ax.title('Analitical detrend')
plt.plot(t,analitical_DET)
plt.plot(t[r_ix],analitical_DET[r_ix])

ax=fig.add_subplot(4,2,4)#median works better
plt.plot(t_win,x_median)
ax=fig.add_subplot(4,2,5)
plt.plot(t_win,x_mean)
ax=fig.add_subplot(4,2,6)#median works better
plt.plot(t_win,x_median_g)
ax=fig.add_subplot(4,2,7)#median works better
plt.plot(t,x_sav)


"I have correlated noise in the detrending."