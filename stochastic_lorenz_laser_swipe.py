# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 21:39:17 2020
***THIS IS WORKING AS IS***
SDE from 
Lorenz laser threshold


Swiping r at some speed

It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 
"""



import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
import scipy.stats as st


def g1(x,sig=0.5):
    return sig

def dW(delta_t):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))


def sde_euler_maruyama(f,g,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])+g(x[i],argsg)*np.random.rand()
    return x,t

"""equation"""
def f1(X, t, gamma, alpha):
    """The Lorenz equations."""
    I, N, A = X
    Ip = -I*(1 - N)
    Np = -gamma*(N-A+I*N) 
    Ap = alpha
    return Ip, Np, Ap


"""parameters an initial conditions"""
gamma=0.1
alpha=0.01

I0=0.00002
N0=0.0002
A0=0

# Maximum time point and total number of time points
# vir=cm.get_cmap('viridis',12)
# sm=plt.cm.ScalarMappable(cmap='viridis',norm=plt.Normalize(vmin=rs[0],vmax=rs[-1]))
sig=0.01
x0=np.random.normal(0,0.002,1)

dt=.001 # time step
T=500
n=int(T/dt)
  
x=np.zeros([n,3])
Is=np.zeros(n)
Ns=np.zeros(n)
As=np.zeros(n)

x[0]=(I0,N0,A0)
Is[0]=I0
As[0]=A0
Ns[0]=N0

    # x,t=sde_euler_maruyama(f1,g1,x0=1,argsf=(r,c,h,k),argsg=(sig))
t=np.linspace(0.,T,n) #time vector
for i in range(n-1):
    Is[i+1] = Is[i]+dt*(-Is[i]*(1 - Ns[i]))
    Ns[i+1] = Ns[i]-gamma*(Ns[i]-As[i]+Is[i]*Ns[i]) 
    As[i+1] =As[i]+dt*alpha+g1(As[i],sig)*dW(dt)
    
    # x[i+1]=x[i]+dt*f1(x[i] ,gamma,alpha)+g1(x[i],sig)*dW(dt)

    # ax.plot(t,x,lw=2,color=vir((rs[-1]-r)/(rs[-1]-rs[0])))
for j in range(len(t)): 
    if As[j]>1: break
ix=j    
    
tO=np.linspace(0.,T,250000) #time vector

f = odeint(f1, (I0, N0, A0), tO, args=(gamma, alpha))
I, N, A = f.T


#%%
fig=plt.figure()
ax=fig.add_subplot(1,1,1)
plt.suptitle('Pitchfork SDE critical slowing down')
ax.plot(t,As-1,'--k',lw=2,alpha=0.9)    
ax.plot(t,Is,color='grey',lw=2,label='SDE')    
ax.plot(tO,I,'--b',lw=1,alpha=0.8,label='ODE')
ax.plot([t[ix], t[ix]],[0, max(Is)],'--r',lw=2,label='Theoretical bifurcation A=cte')    
ax.set_xlabel('Time')
ax.set_ylabel('$y^*$')
ax2=ax.twinx()
# ax2=fig.add_subplot(122)
# ax2.plot(t,As,'r',lw=2)    
ax2.yaxis.label.set_color('red')
ax2.tick_params(axis='y', colors='red')
 # cbar=plt.colorbar(sm)
# cbar.set_label('r')
ax2.set_ylabel('$A$')
ax.legend()
#%%
win_amo=50
x=Is
win=int(len(t)/win_amo)
means=np.zeros(win_amo)
t_metr=np.zeros(win_amo)
stds=np.zeros(win_amo)
RTW=np.zeros(win_amo)
kur=np.zeros(win_amo)
kr2=np.zeros(win_amo)
kr3=np.zeros(win_amo)
rri=np.zeros(win_amo)

for n in range(win_amo):
    means[n]=np.mean(Is[n*win:(n+1)*win])
    t_metr[n]=np.mean(t[n*win:(n+1)*win])
    stds[n]=np.var(Is[n*win:(n+1)*win])
    # RTW[n]=metrics.RTW_max(x[n*win:(n+1)*win])
    kur[n]=st.kurtosis(Is[n*win:(n+1)*win])
    kr2[n]=metrics.kr2(Is[n*win:(n+1)*win])
    kr3[n]=metrics.kr3(Is[n*win:(n+1)*win])
    rri[n]=metrics.metric_rri(x[n*win:(n+1)*win])
    # brtw=boots.bootstrap_func_len(metrics.RTW_max, x[n*win:(n+1)*win], 20)
    RTW[n]=metrics.RTW_max(x[n*win:(n+1)*win])
    # print(n)
    #%%ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)    

fig=plt.figure()
ax2=fig.add_subplot(2,3,1)
plt.suptitle('Pitchfork SDE  window size= %i, t= %.2f' %(win,t[win]) )
ax2.plot(t_metr,rri,'r',lw=2,label='RR1') 
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)  
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,2)
ax2.plot(t_metr,stds,'r',lw=2,label='Variance')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)  
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,3)
ax2.plot(t_metr,RTW,'r',lw=2,label='RTW')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3) 
ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)     
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,4)
ax2.plot(t_metr,kur,'r',lw=2,label='kurtosis')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)  
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,5)
ax2.plot(t_metr,kr2,'r',lw=2,label='kr2')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3) 
ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)     
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,6)
ax2.plot(t_metr,kr3,'r',lw=2,label='kr3')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3) 
ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)     
ax3.set_xlabel('Time')
ax3.set_yticks([])   
    