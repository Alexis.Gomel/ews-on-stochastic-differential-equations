# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 03:30:21 2020

SDE from 
'Factors influecing the detectability of early warning signals of popluation collapes'
paper


It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
from matplotlib import cm

def f1(x,r=1.,c=1.,h=1.,k=100.):
    f=r*x*(1-x/k)-c*(x**2/(x**2+h**2))   
    return f

def g1(x,sig=0.5):
    return sig*x

def dW(delta_t):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))

def sde_euler_maruyama(f,g,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])+g(x[i],argsg)*np.random.rand()
    return x,t


fig,ax=plt.subplots(1,1)
vir=cm.get_cmap('viridis',12)

cs=np.linspace(0.1,5,30)
sm=plt.cm.ScalarMappable(cmap='viridis',norm=plt.Normalize(vmin=cs[0],vmax=cs[-1]))

for c in cs:
      
    r=1
    # c=0.04
    h=1
    k=10
    sig=0.03
    x0=1
    
    dt=.001 # time step
    T=90
    n=int(T/dt)
    
    x=np.zeros(n)
    x[0]=x0
    
    
    # x,t=sde_euler_maruyama(f1,g1,x0=1,argsf=(r,c,h,k),argsg=(sig))
    t=np.linspace(0.,T,n) #time vector
    
    for i in range(n-1):
        x[i+1]=x[i]+dt*f1(x[i] ,r,c,h,k)+g1(x[i],sig)*dW(dt)

    ax.plot(t,x,lw=2,color=vir((cs[-1]-c)/(cs[-1]-cs[0])))

