# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 03:30:21 2020

pitchfork sde

Langevin equation following the 

-simulationg a stochastic differential eq- website.

It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots


sigma=1
mu=10
tau=.05
dt=.0001 # time step


T=5 #integration time
n=int(T/dt) #Number of steps
t=np.linspace(0.,T,n) #time vector
#renormalized variables
sigma_bis=sigma*np.sqrt(2./tau)
sqrtdt=np.sqrt(dt)
x=np.zeros(n)
x2=np.zeros(n)

for i in range(n-1):
    x[i+1]=x[i]+dt*(-(x[i]-mu/tau))+\
        sigma_bis*sqrtdt*np.random.randn()
        
        
for i in range(n-1):
    x2[i+1]=x2[i]+dt*(-(x2[i]-mu/tau))
        
fig,ax=plt.subplots(1,1)
ax.plot(t,x,lw=2)
ax.plot(t,x2,lw=2)

fig,ax=plt.subplots(1,1)
ax.hist(x[:200])

