

# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 03:30:21 2020

SDE from 
'Factors influecing the detectability of early warning signals of popluation collapes'
paper

swipe

It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
from matplotlib import cm
import scipy.stats as st

def f1(x,r=1.,c=1.,h=1.,k=100.):
    f=r*x*(1-x/k)-c*(x**2/(x**2+h**2))   
    return f

def g1(x,sig=0.5):
    return sig

def dW(delta_t):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))

def sde_euler_maruyama(f,g,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])+g(x[i],argsg)*np.random.rand()
    return x,t

# vir=cm.get_cmap('viridis',12)
# sm=plt.cm.ScalarMappable(cmap='viridis',norm=plt.Normalize(vmin=cs[0],vmax=cs[-1]))
      
r=1
# c=0.04
h=1
k=10
sig=0.02
x0=0.1
c0=3*sig

dt=.005 # time step
T=6000
n=int(T/dt)

max_c=5
oscillations=8
c_v=max_c/(T/oscillations)
x=np.zeros(n)
cs=np.zeros(n)

x[0]=x0
cs[0]=c0

# x,t=sde_euler_maruyama(f1,g1,x0=1,argsf=(r,c,h,k),argsg=(sig))
t=np.linspace(0.,T,n) #time vector
for i in range(n-1):
    c=cs[i]
    x[i+1]=x[i]+dt*f1(x[i] ,r,c,h,k)
    if int(t[i]/(T/oscillations)%2)==0:
        vel=c_v
    else: vel=-c_v
    cs[i+1]=abs(cs[i]+dt*vel+g1(cs[i],sig)*dW(dt))
    
fig,ax=plt.subplots(1,1)
ax.plot(t,x,color='grey',lw=2)
ax.set_ylabel('Population')
ax2=ax.twinx()
ax2.plot(t,cs,alpha=0.4)
ax.set_ylabel('Grazing rate')
ax.set_xlabel('time')
#%%
win_data=4000
win_step=int(win_data*0.1)
for i in range(len(t)):
    if win_data+i*win_step>=len(t): break
win_amo=i

win=win_data
means=np.zeros(win_amo)
t_metr=np.zeros(win_amo)
t_centr=np.zeros(win_amo)

stds=np.zeros(win_amo)
RTW=np.zeros(win_amo)
kur=np.zeros(win_amo)
kr2=np.zeros(win_amo)
kr3=np.zeros(win_amo)
rri=np.zeros(win_amo)

for n in range(win_amo):
    means[n]=np.mean(x[n*win_step:n*win_step+win])
    t_metr[n]=np.mean(t[n*win_step+win])
    t_centr[n]=np.mean(t[n*win_step:n*win_step+win])
    stds[n]=np.var(x[n*win_step:n*win_step+win])
    # RTW[n]=metrics.RTW_max(x[n*win:(n+1)*win])
    kur[n]=st.kurtosis(x[n*win_step:n*win_step+win])
    kr2[n]=metrics.kr2(x[n*win_step:n*win_step+win])
    kr3[n]=metrics.kr3(x[n*win_step:n*win_step+win])
    rri[n]=metrics.metric_rri(x[n*win_step:n*win_step+win])
    # brtw=boots.bootstrap_func_len(metrics.RTW_max, x[n*win:(n+1)*win], 20)
    RTW[n]=metrics.RTW_max(x[n*win_step:n*win_step+win])
    # print(n)
    #%%ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)    

fig=plt.figure()
ax2=fig.add_subplot(2,3,1)
plt.suptitle('Pitchfork SDE  window size= %i, t= %.2f' %(win,t[win]) )
ax2.plot(t_metr,rri,'r',lw=2,label='RR1') 
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
# ax3.plot([t[ix], t[ix]],[0, max(x)],'--k',lw=2)  
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,2)
ax2.plot(t_metr,stds,'r',lw=2,label='Variance')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
# ax3.plot([t[ix], t[ix]],[0, max(x)],'--k',lw=2)  
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,3)
ax2.plot(t_metr,RTW,'r',lw=2,label='RTW')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3) 
# ax3.plot([t[ix], t[ix]],[0, max(x)],'--k',lw=2)     
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,4)
ax2.plot(t_metr,kur,'r',lw=2,label='kurtosis')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
# ax3.plot([t[ix], t[ix]],[0, max(x)],'--k',lw=2)  
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,5)
ax2.plot(t_metr,kr2,'r',lw=2,label='kr2')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3) 
# ax3.plot([t[ix], t[ix]],[0, max(Is)],'--k',lw=2)     
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,6)
ax2.plot(t_metr,kr3,'r',lw=2,label='kr3')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3) 
# ax3.plot([t[ix], t[ix]],[0, max(x)],'--k',lw=2)     
ax3.set_xlabel('Time')
ax3.set_yticks([])   
    
