# -*- coding: utf-8 -*-
"""
Created on Mon Nov 29 16:24:02 2021


SDE from Tiping point

It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 
This code integrates an estochastic equation ode of the form f'=r*y*(y**2-1)+noise
This goes from one stable solution at y=0, to two stable solutions y=+-1 when r>0. 
Then plots all the evolutions from the same initlal condition for different parametes, to show the presence
of critical slowing down and compares that to a similar non stochastic ODE. 

THis could be interesting to analize since there is less need to detrending the data. now there is only two constant conditions.
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
from scipy.signal import  savgol_filter 

def f1(y,r=1.):
    return r*(y-2)*(y-1)*(y-3)#r*y*(y**2-1)

def g1(x,sig=0.5):
    return sig

def dW(delta_t):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))


def sde_euler_maruyama(f,g,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])+g(x[i],argsg)*np.random.rand()
    return x,t

def sde_euler(f,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0+np.sqrt(dt)
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])
    return x,t

fig,ax=plt.subplots(1,1)
plt.suptitle('Tipping point SDE critical slowing down')
rs=np.linspace(-4,4,150)
means=np.zeros_like(rs)
means2=np.zeros_like(rs)
var2=np.zeros_like(rs)
rtw2=np.zeros_like(rs)
kr2=np.zeros_like(rs)

ax1=plt.subplot(311)

vir=cm.get_cmap('coolwarm',12)
sm=plt.cm.ScalarMappable(cmap='coolwarm',norm=plt.Normalize(vmin=rs[0],vmax=rs[-1]))

"""time vector"""
dt=.025 # time step
T=100
n=int(T/dt)
t=np.linspace(0.,T,n) #time vector
x=np.zeros(n)
x2=np.zeros(n)

"""rolling window parameters"""
Rwin=500
Rwin_step=20
win=np.zeros(Rwin)
effective_lenght=n-Rwin
n_win=int(effective_lenght/Rwin_step)
T_rolling_window=Rwin*dt
ix=np.linspace(Rwin,(n_win-1)*Rwin_step+Rwin,Rwin_step)
t_win=np.zeros(n_win)
for k in range(n_win):
    t_win[k]=t[k*Rwin_step+Rwin]

print('Rolling window time lenght:  %2.f s' %(T_rolling_window))

""""rolling window variables"""
RWT_win=np.zeros(n_win)
RWTwin_max=np.zeros_like(rs)
RWTwin_2d=np.zeros((len(rs),n_win))

kr2_win=np.zeros(n_win)
kr2win_max=np.zeros_like(rs)
kr2win_2d=np.zeros((len(rs),n_win))

var_win=np.zeros(n_win)
varwin_2d=np.zeros((len(rs),n_win))
var2d=np.zeros((len(rs),n_win))
sig=0.01
for j,r in enumerate(rs):
   
    x0=np.random.normal(2,sig,1)
    x[0]=x0
    x2[0]=x0
         
    for i in range(n-1):
        x[i+1]=x[i]+dt*f1(x[i] ,r)+g1(x[i],sig)*dW(dt)
        x2[i+1]=x2[i]+dt*f1(x2[i] ,r)      

    x2=savgol_filter(x, 150+1, 2)

    for k in range(n_win):
        RWT_win[k]=metrics.RTW_max(x2[k*Rwin_step:k*Rwin_step+Rwin]-x[k*Rwin_step:k*Rwin_step+Rwin])
        kr2_win[k]=metrics.kr2(x2[k*Rwin_step:k*Rwin_step+Rwin]-x[k*Rwin_step:k*Rwin_step+Rwin])
        var_win=np.var(x2[k*Rwin_step:k*Rwin_step+Rwin]-x[k*Rwin_step:k*Rwin_step+Rwin])

    RWTwin_max[j]=np.max(RWT_win)            
    kr2win_max[j]=np.max(kr2_win)            
    RWTwin_2d[j]=RWT_win
    kr2win_2d[j]=kr2_win
    var2d[j]=var_win

    means[j]=np.mean(x)
    means2[j]=np.mean(x2-x)
    var2[j]=np.var(x2-x)
    rtw2[j]=metrics.RTW_max(x2-x)
    kr2[j]=metrics.kr2(x2-x)

    ax1.plot(t,x,lw=2,color=vir((rs[-1]-r)/(rs[-1]-rs[0])))
    ax1.plot(t,x2,lw=1,color='black',alpha=0.4)
    
cbar=plt.colorbar(sm)
cbar.set_label('r')
plt.title('Noise: $\sigma x$')
ax1.set_xlabel('Time')
ax1.set_xlim(0,T)
ax1.set_ylabel('$y^*$')
#%%

ax2=plt.subplot(334)
ax2.plot(rs,var2,lw=2,label='var')
ax2.set_xlabel('$r$')
ax2.legend()
ax2=plt.subplot(335)
ax2.plot(rs,rtw2,lw=2,label='RTW')
ax2.plot(rs,RWTwin_max,lw=2,label='RTW Rwindow max')
ax2.set_xlabel('$r$')
ax2.legend()
ax2=plt.subplot(336)
ax2.plot(rs,kr2,lw=2,label='kr2')
ax2.plot(rs,kr2win_max,lw=2,label='kr2 Rwindow max')
ax2.set_xlabel('$r$')
ax2.legend()


ax=plt.subplot(313)
plt.title('$f=ry(y^2-1)$ bifurcation diagram')
plt.plot([min(rs),0],[0,0],'b')
plt.plot([min(rs),0],[1,1],'--b')
plt.plot([min(rs),0],[-1,-1],'--b')
plt.plot([0,max(rs)],[0,0],'--r')
plt.plot([0,max(rs)],[-1,-1],'-r')
plt.plot([0,max(rs)],[1,1],'-r')
plt.plot(rs,np.flip(means),'*k',label='<y>')
ax.legend()

ax.set_ylabel('$y^*$')
ax.set_xlabel('$r$')


#%%        
plt.figure()
ax=plt.subplot(221)
plt.pcolor(t_win,rs,  RWTwin_2d)
ax.set_ylabel('$r$')
ax.set_xlabel('$t$')
plt.colorbar()
plt.title('RTW rolling window')

ax=plt.subplot(222)
plt.pcolor(t_win,rs,  kr2win_2d,cmap='magma')
ax.set_ylabel('$r$')
ax.set_xlabel('$t$')
plt.colorbar()
plt.title('kr2 rolling window')


ax=plt.subplot(223)
plt.pcolor(t_win,rs, var2d,cmap='inferno')
ax.set_ylabel('$r$')
ax.set_xlabel('$t$')
plt.colorbar()
plt.title('variance rolling window')

# plt.tight_layout()