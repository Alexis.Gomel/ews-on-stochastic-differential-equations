

# -*- coding: utf-8 -*-
"""
Created on Sat Aug 22 12:36:45 2020
SDE from 
'pitchfork'
TWO INITIAL VALUES TEST

Swiping r at some speed

It's a Ornstein-Uhlenbeck process', integrates with a Euler-Marayuma method. 
"""


import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots
import scipy.stats as st
import matplotlib as mp

def f1(y,r=1.):
    return r*y-y**3

def g1(x,sig=0.5):
    return sig*x

def dW(delta_t):
    """Sample a random number at each call."""
    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))


def sde_euler_maruyama(f,g,dt=0.001,T=1,x0=0, argsf=(), argsg=()):
    n=int(T/dt)
    x=np.zeros(n)
    x[0]=x0
    t=np.linspace(0.,T,n) #time vector
    for i in range(n-1):
        x[i+1]=x[i]+dt*f(x[i],argsf[:])+g(x[i],argsg)*np.random.rand()
    return x,t

# vir=cm.get_cmap('viridis',12)
# sm=plt.cm.ScalarMappable(cmap='viridis',norm=plt.Normalize(vmin=rs[0],vmax=rs[-1]))
sig=0.05
x0=np.random.normal(0,0.002,1)

dt=.00002 # time step
T=30
n=int(T/dt)
r0=-20

x=np.zeros(n)
rs=np.zeros(n)
rs[0]=r0
x[0]=x0


max_c=20-r0
oscillations=6
c_v=(max_c)/(T/oscillations)
# v_r=12
# rs_max=r0+v_r*T
# v_r=-2*r0/T #simmetric r0
x2=x+0.0001


    # x,t=sde_euler_maruyama(f1,g1,x0=1,argsf=(r,c,h,k),argsg=(sig))
t=np.linspace(0.,T,n) #time vector
for i in range(n-1):
    r=rs[i]
    x[i+1]=x[i]+dt*f1(x[i] ,r)
    x2[i+1]=x2[i]+dt*f1(x2[i] ,r)
    if int(t[i]/(T/oscillations)%2)==0:
        vel=c_v
    else: vel=-c_v
    rs[i+1]=rs[i]+dt*vel+g1(rs[i],sig)*dW(dt)

    # ax.plot(t,x,lw=2,color=vir((rs[-1]-r)/(rs[-1]-rs[0])))
fig=plt.figure()
ax=fig.add_subplot(1,1,1)
plt.suptitle('Pitchfork SDE critical slowing down')
ax.plot(t,x,lw=2,color='grey')  
ax.plot(t,x2,lw=2,color='red')    
ax.set_xlabel('Time')
ax.set_ylabel('$y^*$')
ax2=ax.twinx()
# ax2=fig.add_subplot(122)
ax2.plot(t,rs,'b',lw=2.5,alpha=0.2)    
ax2.yaxis.label.set_color('blue')
ax2.tick_params(axis='y', colors='blue')
 # cbar=plt.colorbar(sm)
# cbar.set_label('r')
ax2.set_ylabel('$r$')

#%%
win_amo=100
win=int(len(t)/win_amo)
means=np.zeros(win_amo)
t_metr=np.zeros(win_amo)
stds=np.zeros(win_amo)
RTW=np.zeros(win_amo)
kur=np.zeros(win_amo)
kr2=np.zeros(win_amo)
kr3=np.zeros(win_amo)
rri=np.zeros(win_amo)
RTW_c=np.zeros(win_amo,dtype=np.complex64)

for n in range(win_amo):
    means[n]=np.mean(x[n*win:(n+1)*win])
    t_metr[n]=np.mean(t[n*win:(n+1)*win])
    stds[n]=np.var(x[n*win:(n+1)*win])
    # RTW[n]=metrics.RTW_max(x[n*win:(n+1)*win])
    kur[n]=st.kurtosis(x[n*win:(n+1)*win])
    kr2[n]=metrics.kr2(x[n*win:(n+1)*win])
    kr3[n]=metrics.kr3(x[n*win:(n+1)*win])
    rri[n]=metrics.metric_rms_tld(x[n*win:(n+1)*win])
    # brtw=boots.bootstrap_func_len(metrics.RTW_max, x[n*win:(n+1)*win], 20)
    RTW[n]=metrics.RTW_max(x[n*win:(n+1)*win])
    RTW_c[n]=metrics.RTW_c(x[n*win:(n+1)*win])

    # print(n)
    #%%
M_c=RTW_c
fig2=plt.figure()
# fig2.suptitle("Metric for Erf transition. (%.i points).  \n $\sigma/s_2$= %.2f; $\sigma_{n_0}/A_0$= %.2f " %(datalen,sigma/s2, sigma_n0/2), fontsize=14)
ax2 = fig2.add_subplot(111)

ax2.plot(t_metr,np.abs(M_c),'k',label='|RTW_c|')
plt.legend(loc='upper right',bbox_to_anchor=(0.3,1))
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t_metr,np.angle(M_c)/np.pi,'r',label='$\phi (RTW_c)$')
ax3.yaxis.set_major_formatter(mp.ticker.FormatStrFormatter('%g $\pi$'))
plt.legend(loc='upper right',bbox_to_anchor=(0.3,.9))# ax3.set_ylabel('Pareto Normalized')
ax3.yaxis.label.set_color('red')
ax3.tick_params(axis='y', colors='red')
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_yticks([])
# fig2.savefig(savefile+r'\RTW_complex.png')
    
    
    #%%
fig=plt.figure()
ax2=fig.add_subplot(2,3,1)
plt.suptitle('Pitchfork SDE  window size= %i, t= %.2f' %(win,t[win]) )
ax2.plot(t_metr,rri,'r',lw=2,label='RR1') 
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,2)
ax2.plot(t_metr,stds,'r',lw=2,label='Variance')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,3)
ax2.plot(t_metr,RTW,'r',lw=2,label='RTW')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,4)
ax2.plot(t_metr,kur,'r',lw=2,label='kurtosis')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,5)
ax2.plot(t_metr,kr2,'r',lw=2,label='kr2')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])
ax2=fig.add_subplot(2,3,6)
ax2.plot(t_metr,kr3,'r',lw=2,label='kr3')    
ax2.legend()
ax3 = ax2.twinx()  # instantiate a second axes that shares the same x-axis
ax3.plot(t,x,color='grey',lw=2,alpha=0.3)    
ax3.set_xlabel('Time')
ax3.set_yticks([])   
    