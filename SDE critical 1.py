# -*- coding: utf-8 -*-
"""
Created on Fri Aug 21 22:45:59 2020

Over harvesting model from 
'Early warning signals also precede non-catastrophic transitions'

Fold bifurcation: 
    k=10
    r=1
    c=1-3
    V0=1
    
Cursp point:
    k=5.2
    r=1
    c=1-3
    V0=1
    

@author: gomel
"""



import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint
import threshold
import metrics 
import astropy.stats as astrost
import boots

def f1(V,t,r,k,c,V0):
#    r=2.0
    return r*V*(1-V/k)-c*V**2/(V**2+V0**2)


def g1(x,sig=0.5):
    return sig

def dW(delta_t):

    """Sample a random number at each call."""

    return np.random.normal(loc=0.0, scale=np.sqrt(delta_t))


k=100
r=1
c=1
V0=15

r=0.2
y0=1.2
xs = np.linspace(0,50,100)
    #    y0 = 10.0  # the initial condition

sig=0.1
y0=np.random.normal(0,0.002,1)

dt=.025 # time step
T=40
n=int(T/dt)
ys=np.zeros(n)
ys[0]=y0

    # x,t=sde_euler_maruyama(f1,g1,x0=1,argsf=(r,c,h,k),argsg=(sig))
xs=np.linspace(0.,T,n) #time vector
for i in range(n-1):
    ys[i+1]=ys[i]+dt*f1(ys[i],xs,r,k,c,V0)+g1(ys[i],sig)*dW(dt)


'''set up pcolor figures with percentiles'''
fig2=plt.figure(figsize=(10,10))
ax2=fig2.add_subplot(1,1,1)

std=0.1
lims=0.4
amount_of_stats=15
binss=np.linspace(-0.6,0.6,70)

pcolor_matrix=np.zeros((amount_of_stats,len(binss)-1))
pcolor_logmatrix=np.zeros((amount_of_stats,len(binss)-1))
avg_stat_bin=np.zeros((amount_of_stats))
perc_stat_bin_below=np.zeros((amount_of_stats))
perc_stat_bin_above=np.zeros((amount_of_stats))
perc=90#has to be grater than 50

testlim=.35
test_points=np.linspace(-testlim,testlim,amount_of_stats)

j=0
num_points=1000
# fig2.suptitle('Statistics pcolor for r=(%.2f$\pm$%.2f), taken from %.1i shots with a std of %.2f. \n In red is shown the mean and the %.1f and %.1f percentiles ' %(r, r_std,num_points,std,perc,100-perc), fontsize=11, fontweight='bold')


for n,mean in enumerate(test_points):
    j=j+1 
    print(mean)
    initials=[]
    rs=[]
    results=[]
    for num in range(0,num_points):
        y0=np.random.normal(0,0.02,1)
        ys[0]=y0
        V0=mean
        for i in range(n-1):
            ys[i+1]=ys[i]+dt*f1(ys[i],xs,r,k,c,V0)+g1(ys[i],sig)*dW(dt)

        ys = np.array(ys).flatten()
        results.append(ys[-1])

    num,bin_pos=astrost.histogram(results,bins='scott')

    pcolor_matrix[n,:]=num/np.max(num)
    pcolor_logmatrix[n,:]=np.log10(num/np.max(num))
    avg_stat_bin[n]=np.median(results)
    perc_stat_bin_below[n]=np.percentile(results,100-perc)
    perc_stat_bin_above[n]=np.percentile(results,perc)
    

# ax2.set_xlabel('$y*$ at $t=$%.1f' %(t_fin))
ax2.set_ylabel('$<y_0>$')

cmap=ax2.pcolor(binss[:-1],test_points,pcolor_logmatrix)
fig2.colorbar(cmap)
#ax2.plot(avg_stat_bin,test_points,'--r',alpha=0.6,label='mean')
#ax2.fill_betweenx(test_points,perc_stat_bin_below,perc_stat_bin_above,color='red',alpha=0.25)
ax2.legend()